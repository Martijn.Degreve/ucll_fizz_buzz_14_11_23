package be.codekata;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzTests {

    @Test
    public void canConvertBeCalled() {
        FizzBuzz fizzBuzz = new FizzBuzz();
        fizzBuzz.convert(1);
    }

    @Test
    public void canConvertReturn1WhenGiven1() {
        assertFizzBuzzConvert(1, "1");
    }

    @Test
    public void canConvertReturn2WhenGiven2() {
        assertFizzBuzzConvert(2, "2");
    }

    @Test
    public void canConvertReturnFizzWhenGiven3() {
        assertFizzBuzzConvert(3, "Fizz");
    }

    @Test
    public void canConvertReturnBuzzWhenGiven5() {
        assertFizzBuzzConvert(5, "Buzz");
    }

    @Test
    public void canConvertReturn4WhenGiven4() {
        assertFizzBuzzConvert(4,"4");
    }

    @Test
    public void canConvertReturnFizzWhenGiven6() {
        assertFizzBuzzConvert(6,"Fizz");
    }

    @Test
    public void canConvertReturnFizzWhenGiven9() {
        assertFizzBuzzConvert(9, "Fizz");
    }

    @Test
    public void canConvertReturnBuzzWhenGiven10() {
        assertFizzBuzzConvert(10,"Buzz");
    }

    @Test
    public void canConvertReturnBuzzWhenGiven20() {
        assertFizzBuzzConvert(20,"Buzz");
    }


    @Test
    public void canConvertReturnFizzBuzzWhenGiven15() {
        assertFizzBuzzConvert(15, "FizzBuzz");
    }

    @Test
    public void canConvertReturnFizzBuzzWhenGiven30() {
        assertFizzBuzzConvert(30,"FizzBuzz");
    }

    @Test
    public void canConvertReturnFizzBuzzWhenGiven45() {
        assertFizzBuzzConvert(45, "FizzBuzz");
    }

    private void assertFizzBuzzConvert(int input, String expected) {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String value = fizzBuzz.convert(input);
        assertEquals(expected, value);
    }
}
