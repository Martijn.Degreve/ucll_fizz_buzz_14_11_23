package be.codekata;

public class FizzBuzz {
    public String convert(int givenValue) {

        if (isFizzBuzzable(givenValue)) {
            return "FizzBuzz";
        }

        if (isFizzable(givenValue)) {
            return "Fizz";
        }

        if (isBuzzable(givenValue)) {
            return "Buzz";
        }

        return givenValueAsString(givenValue);
    }

    private boolean isFizzBuzzable(int givenValue) {
        return isFizzable(givenValue) && isBuzzable(givenValue);
    }

    private String givenValueAsString(int givenValue) {
        return String.valueOf(givenValue);
    }

    private boolean isBuzzable(int givenValue) {
        return givenValue % 5 == 0;
    }

    private boolean isFizzable(int givenValue) {
        return givenValue % 3 == 0;
    }


}
